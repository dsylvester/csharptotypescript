using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using CSharpToTypescript;

namespace CSharp2Typescript.Test
{
    
    public class UnitTest1
    {
        [Theory]
        [InlineData("c:\\temp\\TSG.CRM.dll")]
        public void LoadAssemblyTest(string name)
        {
            var test = new LoadAssembly(name);

            Xunit.Assert.NotNull(test);
        }
    }
}
