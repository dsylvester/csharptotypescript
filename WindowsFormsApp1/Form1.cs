﻿// using CSharpToTypescript.Legacy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var Dir = new DirectoryInfo(Directory.GetCurrentDirectory());

            var assembly = Assembly.LoadFrom("c:\\temp\\TSG.CRM.dll");

            var ddd = assembly.GetTypes();

            var data = new StringBuilder();
            ddd.ToList().ForEach(x =>
            {
                //if (x.Name.ToLower() == "contact") {
                //    x.GetType().GetProperties().ToList().ForEach(
                //        xx => {
                //            if (xx.PropertyType.IsPublic)
                //            {
                //                data.AppendLine(xx.GetAccessors(x));
                //            }
                //        });
                //}
                // data.AppendLine(x.Name + ": " + x.GetTypeInfo().DeclaredProperties  + " \t: " + ((x.IsInterface) ? "Interface" : ""));

                if (x.IsClass && x.GetProperties().Count() > 0) {
                    data.AppendLine(x.Name);

                    if (x.IsAbstract)
                    {
                        data.AppendLine("Abstract Class");
                    }

                    if (x.BaseType.Name.ToLower() != "object") {
                        data.AppendLine("Has a Base Class: " + x.BaseType.Name);
                    }

                    x.GetProperties().ToList().ForEach(y => {
                        data.AppendLine("\t" + y.Name);
                    });

                }

                data.AppendLine("\n");

            });

            textBox1.Text = data.ToString();
            label1.Text = Directory.GetCurrentDirectory();
            // var aa = new LoadAssembly("c:\\temp\\TSG.CRM.dll");
            var bb = Assembly.Load(new AssemblyName(assembly.FullName));

            var cc = bb.GetTypes();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
