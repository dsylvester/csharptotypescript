﻿using System;
using System.Reflection;

namespace CSharpToTypescript
{
    public class LoadAssembly
    {
        public LoadAssembly(string name) {
            var assembly = Load(name);

        }

        public Assembly Load(string name) {
            try {
                // Assembly.LoadFrom("c:\\Sample.Assembly.dll");
                // Assembly assembly = Assembly.Load(new AssemblyName(name));
                var aa = Assembly.Load(new AssemblyName(name));
                return aa;
            }
            catch (Exception ex) {
                return null;
            }
        }
    }
}
